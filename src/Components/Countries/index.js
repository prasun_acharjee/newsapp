import React from "react";
import styles from "./Countries.module.css";
const Countries=({countryClick,countryList})=>{
    
    const click=(item)=>{
        
        countryClick(item);

    }
    
    return(
        <div className={styles.container}>
            <p className={styles.header}>country</p>
            <div className={styles.lists}>
            {
                countryList.map((country)=><button onClick={()=>click(country)}>{country}</button>)    
            }
            </div>
        </div>
    )
}

export default Countries;