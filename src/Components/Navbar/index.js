import React,{useState} from "react";
import styles from "./Navbar.module.css";

const Navbar =({search})=> {
    
    const [text,setText]=useState('')
    
    const handleChange=(event)=>{
        setText(event.target.value)
    }

    const click=()=>{
        search(text);
        setText('')
    }

    return(
        <div className={styles.container}>
            
            <p className={styles.header}>NEWS</p>
            

            <div className={styles.search}>
                <input placeholder="Search..." onChange={handleChange} value={text} />
                <button onClick={click}>Search</button>
            </div>

        </div>
    )
}

export default Navbar;