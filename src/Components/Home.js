import React,{useState,useEffect} from "react";
import Navbar from "./Navbar/index";
import Categories from "./Categories/index";
import Countries from "./Countries/index";
import NewsList from "./NewsList/index";
import { getCode } from "country-list";
import styles from "./Home.module.css";

const categoryList=[{name:'Business',logo:'far fa-building'},{name:'Technology',logo:'fas fa-microchip'},{name:'Entertainment',logo:'fas fa-film'},{name:'Sports',logo:'far fa-futbol'},{name:'Health',logo:'fas fa-first-aid'},{name:'Science',logo:'fas fa-flask'}]
const countryList=['India','Argentina','Brazil','Belgium','Spain','Italy','Turkey','Japan','China','Hong Kong','Netherlands','Greece','Canada','Mexico','Germany','Australia','Austria','Peru','Ukraine','Iceland','Ireland','Egypt','Cuba',
'Colombia','Portugal']

const Home = () =>{
    
    const [newsArray,updateArray]=useState([]);

    useEffect(()=>{
        fetch(`https://newsapi.org/v2/top-headlines?country=in&apiKey=86e2464e6dfe4afe89b811527a204863`).then((response)=>{
         return response.json();
        }).then((json)=>{
            updateArray(json.articles)
        }).catch(()=>{
            console.log("Error in fetch")
        })
    },[])

    const search=(query)=>{
        if(query)
        {
            fetch(`https://newsapi.org/v2/everything?q=${query}&apiKey=86e2464e6dfe4afe89b811527a204863`).then((response)=>{
                return response.json();
               }).then((json)=>{
                   updateArray(json.articles)
               }).catch(()=>{
                   console.log("Error in fetch")
               })
        }
        else
            alert('Empty search field');
    }

    const categories=(query)=>{
        fetch(`https://newsapi.org/v2/everything?q=${query}&sortBy=popularity&apiKey=86e2464e6dfe4afe89b811527a204863`).then((response)=>{
         return response.json();
        }).then((json)=>{
            updateArray(json.articles)
        }).catch(()=>{
            console.log("Error in fetch")
        })
    }
    const countries=(query)=>{

        let countryCode=getCode(query)

        fetch(`https://newsapi.org/v2/top-headlines?country=${countryCode}&sortBy=popularity&apiKey=86e2464e6dfe4afe89b811527a204863`).then((response)=>{
         return response.json();
        }).then((json)=>{
            updateArray(json.articles)
        }).catch(()=>{
            console.log("Error in fetch")
        })
    }

    return(
        <div>
            <Navbar search={search}/>
            <div className={styles.mainContainer}>
                <Categories categoryList={categoryList} categoryClick={categories}/>
                <NewsList newsArray={newsArray}/>
                <Countries countryList={countryList} countryClick={countries}/>
            </div>
        </div>
    )
}

export default Home;